// P.D.E INSTITUTO SAN JOSE A-355
// ING. GABRIEL GARCIA
// EJERCICIO: HAGA UN ALGORITMO PARA CONVERTIR GRADOS CELSIUS EN FAHRENHEIT


#include<stdio.h>

int main()
{

    float celsius, fahrenheit;
    printf("\n\nIngrese la Temperatura en Grados Celsius: ");
    scanf("%f", &celsius);

    fahrenheit = (1.8*celsius) + 32;

    printf("\n\n\nTemperatura en Fahrenheit is: %f ", fahrenheit);

    return 0;
}
